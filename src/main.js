import Vue from 'vue';
import VueResource from 'vue-resource';
import router from './router';
import App from './App';

var tracker;

Vue.use(VueResource);

Vue.http.options.root = 'https://www.imagemakers.co.za/wp-json';

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },  
  mounted: function() {

  }
});

function crumb() {
  var beep = $('.menu .active')[0].innerText;
  if(beep !== 'HOME') {
    $('.indicatorJones').text(beep).show();       
  } else {
    $('.indicatorJones').hide();
  }       
}

function updateIndicator(slideIndex, anchorLink, slideAnchor) {    

    var sa = ' / '+slideAnchor;
    if (slideAnchor === undefined) { sa='';}

    if(slideIndex !== 0) {


      // if(slideAnchor === 'contact')
      // console.log(anchorLink);

      $('#indicator').text( anchorLink + sa).show('fast');

    } else {
      $('#indicator').text( anchorLink).show('fast');
    }

}

function activateActiveState(thisthing) {

    tracker = $(thisthing).find('.slide.active');
    tracker.addClass('loadImage'); 

}

function reCheckLayout() {

    setTimeout(function(){
      $.fn.fullpage.reBuild()
    }, 500);    

}

function repositionDigitalCatalogue() {

  var diff = 0.5;
  var vwidth = $('#myViewerContent').width();
  var vheight = vwidth/diff;
  if(vheight > 800){
    vheight = 800;
  }
  $('#myViewerContent').css({"height": vheight+"px"});

}

function checkCatalogue(anchorLink) {

  if(anchorLink === 'online-catalogue'){
    $($('#catalogueMask')[0]).show();
  }  else {
    $($('#catalogueMask')[0]).hide();
  }

}

function slideNavigator(reference) {

  let frame = $('.'+reference+'-frame')

  let defaults = {
    scrollBar: $('.'+reference+'-scrollbar'),
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,    
    scrollBy: 1,
    activatePageOn: 'click',
    speed: 200,
    moveBy: 600,
    elasticBounds: 1,
    dragHandle: 1,
    dynamicHandle: false,
    clickBar: 1,
  }

  let stuff = {}

  return new Sly( frame, defaults, stuff )
}
 
let teamLookNavigator = slideNavigator('tl');
let maxMatchNavigator = slideNavigator('mm');
let styleGuideNavigator = slideNavigator('sg');
let colourChartNavigator = slideNavigator('cc');

teamLookNavigator.init();
maxMatchNavigator.init();
styleGuideNavigator.init();
colourChartNavigator.init();

$('.mm_browse, .sg_browse, .tl_browse').slick({
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  arrows: true,
  prevArrow: '<button type=\"button\" class=\"slick-prev\"> &lsaquo;</button>',
  nextArrow: '<button type=\"button\" class=\"slick-next\"> &rsaquo;</button>',
  responsive: [
    {
      breakpoint: 2800,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: false
      }
    },  
    {
      breakpoint: 2100,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false
      }
    },
    {
      breakpoint: 1400,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false
      }
    },    
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false
      }
    }
  ]
});

$('.more-info').mouseover(function() {        
    $(this).css('background-color','#ffffff');
    $(this).find('.code, .description, .sizes ').css('visibility','visible');
}).mouseout(function() {
    $(this).css('background-color','transparent'); 
    $(this).find('.code, .description, .sizes ').css('visibility','hidden');
});

$('#alles').fullpage({
  anchors: [
    'home', 
    'team-looks', 
    'style-guide', 
    'mix-and-match',             
    'colour-chart',
    'size-chart',
    'online-catalogue',
    'order-your-free-catalogue',
    'make-an-enquiry',
    'get-a-call-back-in-60-seconds',
    //
    'contact',
    'clients',
    'privacy-policy',
    'terms-and-conditions',
    'frequently-asked-questions',
    'delivery-policy',
    'exchange-policy',

      ],
    resize : true,
    scrollingSpeed: 700,
    css3: false,
    menu: '.menu',
    responsive: true,
    scrollOverflow: true,  
    scrollOverflowOptions: {
        disablePointer: true
      }, 
    fitSection: true,
    slidesNavigation: true,
    slidesNavPosition: 'bottom',
    lazyLoading: true,
    touchSensitivity: 50,
    continuousVertical: true,
    paddingTop: '73px',
    paddingBottom: '0px',
    afterRender: function(anchorLink, index, slideAnchor, slideIndex, destination) {                    

      //

    },

    onLeave: function(slideIndex, anchorLink, slideAnchor, index, nextIndex, direction, destination){             
      
      // 

    },

    afterLoad: function(anchorLink, index, slideAnchor, slideIndex, destination){

        // console.log('do something');

        var _this = $(this);
        activateActiveState(_this);     
       
       if(slideIndex !== undefined) {

          //updateIndicator(slideIndex, anchorLink, slideAnchor);

       }

       crumb();

    },

    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex, destination) {  

        var _this = $(this);
        activateActiveState(_this);  

        reCheckLayout();

        //updateIndicator(slideIndex, anchorLink, slideAnchor);        

        crumb();

    },

 });

setTimeout(function(){

  $('.spinner').fadeOut();
  $("#loading").fadeOut(2000);

}, 1000);

$(window).resize(function(e) {

  // this is expensive but necessary for now
  $.fn.fullpage.reBuild();

  // functions to fire relative to page state
  teamLookNavigator.reload();
  maxMatchNavigator.reload();
  styleGuideNavigator.reload();
  colourChartNavigator.reload();
  repositionDigitalCatalogue();

});