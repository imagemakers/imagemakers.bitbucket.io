import Vue from 'vue'
import Router from 'vue-router'

// Root
import Home from '@/components/home'

// Main Menu
import TeamLook from '@/components/team-look'
import MixMatch from '@/components/mix-match'
import ColourChart from '@/components/colour-chart'
import StyleGuide from '@/components/style-guide'
import SizeChart from '@/components/size-chart'
import DigitalCatalogue from '@/components/digital-catalogue'

// Sub Menu
import Contact from '@/components/contact'
import Careers from '@/components/careers'
import About from '@/components/about'
import CSR from '@/components/csr'
import Privacy from '@/components/privacy'
import Terms from '@/components/terms'
import FAQ from '@/components/faq'
import Deliveries from '@/components/deliveries'
import Exchanges from '@/components/exchanges'

// Forms
import FormCallback from '@/components/form-callback'
import FormEnquire from '@/components/form-enquire'
import FormOrderCatalogue from '@/components/form-order-catalogue'

// Errors
import Beep from '@/components/beep'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/',                              name: 'Root',               component: Home               },
  	{	path: '/home',	                        name: 'Home',  		          component: Home               },
        
    { path: '/team-look',                     name: 'Team Look',          component: TeamLook,           
                                              meta: {
                                                  title:    'Team Look',
                                                  metaTags: [{
                                                    name:     'beep description',
                                                    content:  'beep content'
                                                  }]
                                              } },
    { path: '/mix-and-match',                 name: 'Mix and Match',      component: MixMatch           },     
    { path: '/colour-chart',                  name: 'Colour Chart',       component: ColourChart        },   
    { path: '/style-guide',                   name: 'Style Guide',        component: StyleGuide         }, 
    { path: '/size-chart',                    name: 'Size Chart',         component: SizeChart          }, 
    { path: '/online-catalogue',              name: 'Digital Catalogue',  component: DigitalCatalogue   }, 

  	{	path: '/contact', 		                  name: 'Contact', 	          component: Contact	          },
    {	path: '/careers',       		            name: 'Careers', 	          component: Careers	          },   
    { path: '/about',                         name: 'About',              component: About              },   
    { path: '/social-responsibility',         name: 'CSR',                component: CSR                },   
    { path: '/privacy-policy',                name: 'Privacy',            component: Privacy            },   
    { path: '/terms-and-conditions',          name: 'Terms',              component: Terms              },   
    { path: '/frequently-asked-questions',    name: 'FAQ',                component: FAQ                },   
    { path: '/deliveries',                    name: 'Deliveries',         component: Deliveries         },   
    { path: '/exchanges',                     name: 'Exchanges',          component: Exchanges          },  

    { path: '/order-your-free-catalogue',     name: 'Order Your Free Catalogue',          component: FormOrderCatalogue          }, 
    { path: '/make-an-enquiry',               name: 'Make an Enquiry',                    component: FormEnquire          }, 
    { path: '/get-a-call-back-in-60-seconds', name: 'Get a Callback in 60 seconds',       component: FormCallback          },    

    { path: '/*/',                            name: 'Beep',               component: Home               },           	  	    
  ],
  mode: 'history'
})